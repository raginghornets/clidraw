#!/usr/bin/env python3

import draw

# Draw a filled in rectangle given x as width and y as height
fill_rect = lambda x,y: '\n'.join([draw.horz_line(x) for _ in range(abs(y))])

# Draw the border of a rectangle given x as width and y as height 
def rect(x,y):
  # Rectangle can't have a width or height of 0 or less
  if x <= 0 or y <= 0:
    return ''

  # Add the first line
  res = draw.horz_line(x)
  if y > 1:
    res += '\n'

  # Add the middle part
  if y > 2 and x > 1:
    res += draw.two_vert_lines(x,y) + '\n'
  elif y > 2 and x < 2:
    res += draw.vert_line(y - 2) + '\n'
  
  # Add the last line
  if y > 1:
    res += draw.horz_line(x)

  return res

if __name__ == '__main__':
  import sys
  try:
    arg1 = int(sys.argv[1])
    arg2 = int(sys.argv[2])
    print(fill_rect(arg1, arg2))
    print(draw.divider(arg1))
    print(rect(arg1, arg2))
  except:
    print('Usage: rect.py [integer] [integer]')
