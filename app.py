#!/usr/bin/env python3

from clidraw import rect

def get_int(s):
  res = raw_input(s)
  while not res.isdigit():
    res = raw_input(s)
  return int(res)

if __name__ == '__main__':
  x = get_int('Enter rectangle width: ')
  y = get_int('Enter rectangle height: ')
  print('\nHere\'s your rectangle!\n')
  print(rect.rect(x,y))